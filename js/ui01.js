var app = app || {};


app.fadePage = function(){
    TweenMax.from('#wrap',1,{opacity:0});
};

app.menu = function () {
    var $menuBtn = $('.menuBtn'),
        $closeBtn = $('.navClose'),
        $gnb = $('#gnb'),
        $dim = $('.dim');
    $menuBtn.click(function () {
        $gnb.addClass('navOn').animate({ 'right': '0' });
        $closeBtn.animate({ 'right': '85%' });
        setTimeout(function () {
            $dim.animate({ 'opacity': '1' });
        }, 400);
    });
    $closeBtn.click(function () {
        $gnb.animate({ 'right': '-100%' });
        $closeBtn.animate({ 'right': '-100%' });
        $dim.animate({ 'opacity': '0' }, 700);
        setTimeout(function () {
            $gnb.removeClass('navOn');
        }, 800);
    });
};

app.contentTab = function(){
    var $tab = $('.tab li');
    var $tabInner = $('.tabContents > div');
    $tabInner.hide();
    $tab.click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        $tabInner.hide();
        var idx = $(this).index();
        $tabInner.eq(idx).fadeIn();
        var width = $(this).position().left;
        $('.tab span').css('left',width);
    });
    $tabInner.eq(0).show();
};

app.heart = function(){
    var $heart = $('.infoBtn span').eq(0);
    $heart.click(function(){
        $(this).toggleClass('on');
    });
};

app.search = function(){
    var $search = $('.search').find('label'),
        $searchTxt = $('.search').find('input');
    $search.click(function(){
        $searchTxt.toggleClass('on');
    });
};

app.map = function(){
    var $mapWrap = $('.galleryLocation'),
        $mapPoint = $mapWrap.find('.subTab li'),
        $infoWrap = $mapWrap.find('.subTabContents'),
        $infoLi = $infoWrap.find('.location'),
        activeClass = 'active',
        $closeBtn = $('.closeBtn');
    
    $mapPoint.on('click', function(){
        var idx = $(this).index();
        $(this).addClass(activeClass).siblings().removeClass(activeClass);
        $infoLi.eq(idx).show().siblings().hide();
    });
    $closeBtn.on('click', function(){
        $(this).closest('.location').hide();
        $mapPoint.removeClass(activeClass);
    });
    
    //맵 초기화
    $('.tabWrap .tab ul li').on('click', function(){
        $infoLi.hide();
        $mapPoint.removeClass(activeClass);
    })
}

app.gallery = function(){
    var $galleryWrap = $('.galleryInfo'),
        $gallery = $galleryWrap.find('.tabs li'),
        activeClass = 'active';

    $gallery.on('click', function(){
        var galleryName = $(this).attr('data-dong'),
            gallery = $('.infoGall[data-gallery="' + galleryName + '"]');

            $('.infoGall').hide();
            gallery.show()

            $(this).addClass(activeClass).siblings().removeClass(activeClass);
            if( galleryName === 'all' ){
                $('.infoGall').show();
            }
    });
}

app.scroll = function(){
    var _this = this;
    _this.$animate = _this.$body.find(".sec");
    var sec1,sec2,sec3 = null;

    var handler = {
      sec1: function SEC1_ANIMATE() {
        if(sec1) return;
        sec1 = true;
        var $container = $("#sec1"), tl = new TimelineMax();
        $container.find(".swiper-slide-active > a").each(function(idx){
            var _this = this;
            if(idx === 0){
                tl.to($(_this).find("img"), .5, { x:0, opacity:1, ease: Power3.easeOut }, .3)
                .to($(_this).find(".txt"), .5, { x:0, opacity:1, ease:Power3.easeOut}, .7)
            }else if(idx === 1){
                tl.to($(_this).find("img"), .5, { x:0, opacity:1, ease: Power3.easeOut}, .4)
                .to($(_this).find(".txt"), .5, { x:0, opacity:1, ease:Power3.easeOute}, .9)
            }else{
                tl.to($(_this).find("img"), .5, { x:0, opacity:1, ease: Power3.easeOut }, .5)
                .to($(_this).find(".txt"), .5, { x:0, opacity:1, ease:Power3.easeOut}, 1.2)
            } 
        });
               
      },
      sec2: function SEC2_ANIMATE() {
        if(sec2) return;
        sec2 = true;
        var $container01 = $(".gallerySlide02"),
            tl = new TimelineMax();

            $container01.find(".swiper-slide-active > a").each(function(idx){
                var _this = this;
                if(idx === 0){
                    tl.to($(_this), .3, {className:'+=on'}, .3)
                }else if(idx === 1){
                    tl.to($(_this), .3, {className:'+=on'}, .4)
                }else{
                    tl.to($(_this), .3, {className:'+=on'}, .5)
                }
                
            })
      },
      sec3: function SEC3_ANIMATE() {
        if(sec3) return;
        sec3 = true;
        var $container01 = $(".gallerySlide03"),
            tl = new TimelineMax();

            $container01.find(".swiper-slide > a").each(function(idx){
                var _this = this;
                if(idx === 0){
                    tl.to($(_this), .3, {className:'+=on'}, .3)
                }else if(idx === 1){
                    tl.to($(_this), .3, {className:'+=on'}, .4)
                }else if(idx === 2){
                    tl.to($(_this), .3, {className:'+=on'}, .5)
                }else{
                    tl.to($(_this), .3, {className:'+=on'})
                }
                
            })
      }
      
    }
    
    $(window).on("scroll", function(){
        var scrollTop = (window.innerHeight / 2) + (window.innerHeight / 3);
        _this.$animate.each(function(){
            var eleTop = $(this)[0].getBoundingClientRect().top;
            if(eleTop < scrollTop) {
                var id = $(this).attr("id");
                handler[id]();
            }
        });
    }).trigger("scroll");
}

$(document).on('ready', function(){
    app.$body = $("body");
    app.scroll();
    app.fadePage();
    app.contentTab();
    app.heart();
    app.menu();
    app.search();
    app.map();
    app.gallery();
});

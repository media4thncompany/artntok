var art02 = {
	scroll: function() {
	  var _this = this;
	  _this.$animate = _this.$body.find(".animate");
  
	  var mainVisual, 
	  	  sec2, 
	  	  sec3, 
	  	  sec4,
	  	  sec5,
	  	  sec6,
		sec602,
		sec603,
		sec604,
	  	  sec7,
	  	  sec8 = null;
  
	  var handler = {
		mainVisual: function mainVisualAni() {
		  if(mainVisual) return;
		  mainVisual = true;
		  var $container = $(".visualWrap"), tl = new TimelineMax();
		  tl.fromTo($container.find(".swiper-slide:nth-child(1)"), .3, {x:100 + '%', opacity:1, ease: Linear.easeNone }, {x:0 + '%', ease: Linear.easeNone})
		  .fromTo($container.find(".swiper-slide:nth-child(3)"), .8, { x:-120 + '%', opacity:1, delay:.55, ease: Linear.easeNone }, { x:0 + '%', opacity:0.5, ease: Linear.easeNone })
		  .fromTo($container.find(".swiper-slide:nth-child(2)"), .8, { x:-40 + '%' , opacity:1, delay:.55, ease: Linear.easeNone }, { x:0 + '%', opacity:0.5, ease: Linear.easeNone })
		  .to($container.find(".swiper-slide:nth-child(2)"), .1, {opacity:1, ease: Linear.easeNone })
		  .to($container.find(".swiper-slide:nth-child(3)"), .1, {opacity:1, ease: Linear.easeNone })
		},
		sec2: function sec2Ani() {//overview
		  if(sec2) return;
		  sec2 = true;
		  var $container = $(".list"), tl = new TimelineMax();
		  tl.to($container.find("li:first-child"), .4, {scaleX:1, scaleY:1, opacity:1, delay:.35, ease: Back.easeOut })
		  .to($container.find("li:nth-child(2)"), .4, {scaleX:1, scaleY:1, opacity:1, ease: Back.easeOut })
		  .to($container.find("li:nth-child(3)"), .4, {scaleX:1, scaleY:1, opacity:1, ease: Back.easeOut })
		},
		sec3: function sec3Ani() {//01 IMPROVEMENT
		  if(sec3) return;
		  sec3 = true;
		  var $container = $(".impWrap01"), tl = new TimelineMax();
		  tl.to($container.find(".bg"), .7, { x:0, delay:.3, ease: Power4.easeOut })
		  .to($container.find(".left"), .7, { x:0, opacity:1, ease: Power4.easeOut })
		  .to($container.find(".con01"), .5, { x:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".con02"), .3, { x:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".con03"), .3, { x:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".con04"), .3, {x:0, opacity:1, ease: Linear.easeNone })
		  
		},
		sec4: function sec4Ani() {
		  if(sec4) return;
		  sec4 = true;
		  var $container = $(".impWrap02"), tl = new TimelineMax();
		  tl.to($container.find(".right"), .4, { x:0, opacity:1, ease: Power1.easeOut })
		  .to($container.find(".imgW"), .4, { x:0, opacity:1, delay:.55, ease: Power1.easeOut })
		},
		sec5: function sec5Ani() {
		  if(sec5) return;
		  sec5 = true;
		  var $container = $(".impWrap03"), tl = new TimelineMax();
		  tl.to($container.find(".bg"), .55, { x:0, delay:.35, ease: Power4.easeOut })
		  .to($container.find(".left"), .5, { x:0, opacity:1, ease: Power1.easeOut })
		  .to($(".impWrap03 .imgW .btn.btnPause"), .3, { opacity:0.5, ease: Linear.easeNone })
		  .to($(".right p:first-child"), .4, { x:0, opacity:1, ease: Linear.easeNone })
		  .to($(".right p:nth-child(2)"), .4, { x:0, opacity:1, ease: Linear.easeNone })
		  .to($(".impWrap03 .imgW .btn.btnPause"), .8, { opacity:0, delay:.8, ease: Linear.easeNone })
		},
		sec6: function sec6Ani() {
		  if(sec6) return;
		  sec6 = true;
		  var $container = $(".history"), tl = new TimelineMax();
		  tl.to($container, .5, { y:0, opacity:1, delay:.55, ease: Linear.easeNone })
		},
		sec602: function sec602Ani() {
		  if(sec602) return;
		  sec602 = true;
		  var $container = $(".footPrint"), tl = new TimelineMax();
		  tl.to($container, .5, { y:0, opacity:1, delay:.55, ease: Linear.easeNone })
		},
		sec603: function sec603Ani() {
		  if(sec603) return;
		  sec603 = true;
		  var $container = $(".kukLife"), tl = new TimelineMax();
		  tl.to($container, .5, { y:0, opacity:1, delay:.55, ease: Linear.easeNone })
		},
		sec604: function sec604Ani() {
		  if(sec604) return;
		  sec604 = true;
		  var $container = $(".mobileV"), tl = new TimelineMax();
		  tl.to($container, .5, { y:0, opacity:1, delay:.55, ease: Linear.easeNone })
		},
		sec7: function sec7Ani() {
		  if(sec7) return;
		  sec7 = true;
		  var $container = $(".pcConWrap"), tl = new TimelineMax();
		  tl.to($container.find(".left"), .5, { y:0, opacity:1, delay:.55, ease: Linear.easeNone })
		  .to($container.find(".right"), .5, { y:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".left"), .5, { y:0, opacity:1, ease: Linear.easeNone })
		},
		sec8: function sec8Ani() {
		  if(sec8) return;
		  sec8 = true;
		  var $container = $(".mConWrap"), tl = new TimelineMax();
		  tl.to($container.find(".cir"), .4, { scaleX:1, scaleY:1, delay:.35, opacity:1, ease: Power1.easeOut })
		  .to($container.find(".left"), .4, { y:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".mid"), .4, { y:0, opacity:1, ease: Linear.easeNone })
		  .to($container.find(".right"), .4, { y:0, opacity:1, ease: Linear.easeNone })
		}
	  }
	
	$('#header .btnMenu').on('click', function(){
		$('body').addClass('scrollStop');
	});
	$('#header .btn.close').on('click', function(){
		$('body').removeClass('scrollStop');
	});
	$('.conWrap .conTop').on('click', function(){
		if($(this).parent().hasClass('extend')){
			$(this).parent().removeClass('extend');
			$('.conMid').animate({
				marginTop:0,
			},100);
		}else{
			$(this).parent().addClass('extend');
		}
	});
	$('.conWrap .before').on('click', function(){
		if($(this).parent().hasClass('extend')){
			$(this).parent().removeClass('extend');
			$(this).parent().find('.conMid').removeClass('extend');
		}else{
			$(this).parent().addClass('extend');
		}
	});
	$('.conFoot .talk').on('click', function(){
		$(this).parents('.conWrap').addClass('extend');
		$('html, body').scrollTop(0);
		$('.tabCon').hide();
		$('.conMid .tab ul li').removeClass('on');
		$('.conMid .tab ul li').eq(2).addClass('on');
		$('.tabCon:eq(2)').show();
	});

	$('.conMid .tab ul li').on('click', function(){
		if(!($('.conWrap .conTop').hasClass('extend'))){
			$('.conWrap').addClass('extend');
			$('html, body').scrollTop(0);
		}
		var tabLiNum = $('.tab ul li').index(this);
		$('.conMid .tab ul li').removeClass('on');
		$(this).addClass('on');
		$('.tabCon').hide();
		$('.tabCon:eq('+ tabLiNum +')').show();
		$('.conMid').animate({
			marginTop:-$('.conTop').height()-20,
		},100);
	});

	$('.btnWrite').on('click', function(){
		$(this).parents('.textA01').find('textarea').addClass('on');
		$('.btnWrite').hide();
		$('.btnWrite002').show();
		$('#sub0201TextA').focus();
	});

	$('#sub0201TextA').on('focusout', function(){
		$(this).removeClass('on');
			console.log($('#sub0201TextA').val());
		if($('#sub0201TextA').val() !== ''){
			$('.btnWrite').hide();
			$('.btnWrite002').show();
		}else{
			$('.btnWrite').show();
			$('.btnWrite002').hide();
		}
	});
	$('#sub0201TextA').on('focusin', function(){
		$(this).addClass('on');
	});
	$('.tab02 li').on('click', function(){
		$(this).addClass('active').siblings().removeClass('active');
	});
	
	var swiper = new Swiper('.main .visualWrap.swiper-container', {
	pagination: {
	el: '.swiper-pagination',
		clickable: true,
	},
	autoplay: {
		delay: 4000,
	},
	//effect: 'fade',
	// on: {
	// 	click: function () {
	// 	console.log('click');
	// 	},
	// }
	});
	var swiper2 = new Swiper('.main .listType01.swiper-container', {
		spaceBetween: 8,
		slidesPerView: 'auto'
	});
	var swiper3 = new Swiper('.main .listType02.swiper-container', {
		spaceBetween: 8,
		slidesPerView: 'auto'
	});
	var swiper4 = new Swiper('.main .listType03.swiper-container', {
		spaceBetween: 8,
		slidesPerView: 'auto'
	});
	var swiper5 = new Swiper('.main .perList.swiper-container', {
		spaceBetween: 16,
		slidesPerView: 'auto'
	});
  
	  function handleTweenPage() {
		TweenMax.to(this, 1, { y:0, opacity:1, ease: Linear.easeNone })
	  }
  
	  $(window).on("scroll", function(){
		var viewTop = $(this).scrollTop(),
			viewHeight = $(this).outerHeight(true),
			viewBottom = viewTop + viewHeight;
  
		_this.$animate.each(function(){
		  var elementTop = $(this).offset().top,
			  elementHeight = $(this).outerHeight(),
			  elementBottom = elementTop + elementHeight;
  
		  if(viewTop < elementBottom && viewBottom > elementTop) {
      var id = $(this).closest(".section").attr("id");
      console.log(id);
			if($(this).hasClass("page")) {
			  handleTweenPage.call(this);
			} else {
			  handler[id]();
			}
		  }
		})
	  }).trigger("scroll");
	}
  }

$(function(){
	art02.$body = $("body");
  	art02.scroll();
})
